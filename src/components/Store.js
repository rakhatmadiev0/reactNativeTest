import React from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import Search from './SearchPage/Search';
import Item from './SearchPage/Item';
import Filter from  './SearchPage/Filter';

const Store = () => {
  const productsData = [
    { id: '1', name: 'Пример товара 1', brand: 'Бренд 1', category: 'Категория 1', price: 100,  },
    { id: '2', name: 'Пример товара 2', brand: 'Бренд 2', category: 'Категория 2', price: 150,  },
    { id: '3', name: 'Пример товара 3', brand: 'Бренд 1', category: 'Категория 1', price: 100,  },
    { id: '4', name: 'Пример товара 4', brand: 'Бренд 1', category: 'Категория 1', price: 100,  },
    { id: '5', name: 'Пример товара 5', brand: 'Бренд 1', category: 'Категория 1', price: 100,  },
    { id: '6', name: 'Пример товара 6', brand: 'Бренд 1', category: 'Категория 1', price: 100,  },
  ];

  return (
    <ScrollView>
      <View style={styles.container}>
        <View style={styles.header}>
          <Search />
        </View>
        <View style={styles.underline}></View>
        <Text style={styles.title}><b>Фильтр</b></Text>
        <Filter />
        <View style={styles.underline}></View>

        {productsData.map((product) => (
          <Item
            key={product.id}
            data={{
              ...product,
              customImage: '../../../assets/images/item.png',
            }}
          />
        ))}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 247.98,
    height: 35.49,
    top: 60.09,
    left: 25.21,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: 'gray',
    marginBottom: 70,
  },
  underline: {
    height: 0.5,
    backgroundColor: 'gray',
    marginBottom: 25,
    marginLeft: 16,
    marginRight: 16,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
});

export default Store;
