import * as React from "react"
import Svg, { Circle, Path } from "react-native-svg"
const Next = (props) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={79}
    height={79}
    fill="none"
    {...props}
  >
    <Circle cx={39.5} cy={39.535} r={39.185} fill="#AF9778" />
    <Path
      fill="#fff"
      d="M27.951 37.859a1.522 1.522 0 0 0-.04 3.044l.04-3.044Zm24.18 2.92c.602-.586.615-1.55.029-2.152l-9.556-9.814a1.522 1.522 0 0 0-2.181 2.124l8.494 8.723-8.724 8.494a1.522 1.522 0 0 0 2.124 2.181l9.814-9.556Zm-24.22.123 23.138.309.04-3.044-23.138-.308-.04 3.044Z"
    />
  </Svg>
)
export default Next
