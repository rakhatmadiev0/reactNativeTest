import * as React from 'react';
import { Image, View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { login } from '../api/main';


/* SVG IMPORTS */
import ImgLogo from '../../assets/icons/ImgLogo';
/* SVG IMPORTS */


export default function LoginPage() {
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');

  const handleLogin = async () => {
    try {
      const result = await login(username, password);
      console.log('Login successful:', result);
    } catch (error) {
      console.error('Login failed:', error);
    }
  };
  
  return (
    <View style={styles.container}>
        <View style={styles.image}>
            <ImgLogo/>
        </View>
      <View style={styles.formContainer}>
        <Text style={styles.welcomeText}>Добро Пожаловать</Text>
        <View style={styles.inputContainer}>
          <Icon name="user" size={20} color="black" style={styles.icon} />
          <TextInput
            style={styles.input}
            placeholder="Логин"
            placeholderTextColor="black"
            value={username}
            onChangeText={(text) => setUsername(text)}
          />
        </View>
        
        <View style={styles.inputContainer}>
          <Icon name="lock" size={20} color="black" style={styles.icon} />
          <TextInput
            style={styles.input}
            placeholder="Пароль"
            placeholderTextColor="black"
            value={password}
            onChangeText={(text) => setPassword(text)}
            secureTextEntry
          />
        </View>
        
        <TouchableOpacity style={styles.loginButton} onPress={handleLogin}>
          <Text style={styles.buttonText}>Войти</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  formContainer: {
    width: '80%',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 12,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 10,
    padding: 8,
  },
  icon: {
    marginRight: 10,
  },
  input: {
    flex: 1,
    height: 40,
  },
  loginButton: {
    backgroundColor: '#183961',
    padding: 15,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    width: 315,
    height: 48.26,
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    
  },
  welcomeText: {
    fontSize: 23,
    fontWeight: '700',
    textAlign: 'center',
    marginBottom: 30,
  },
});
