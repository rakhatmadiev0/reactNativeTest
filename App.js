import React, { useState } from 'react';
import { SafeAreaView, View, Text, StyleSheet } from 'react-native';
import { useFonts } from 'expo-font';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
// import Svg, { Path } from "react-native-svg"
// import Checkbox from 'expo-checkbox';

import IntroductionPage from './src/components/IntroductionPage';
import MainPage from './src/components/MainPage';
import Store from './src/components/Store';
import LoginPage from './src/components/LoginPage';

const Stack = createStackNavigator();

export default function App() {  
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Store" screenOptions={{
          headerShown: false, 
        }}>
        <Stack.Screen name="IntroductionPage" component={IntroductionPage} />
        <Stack.Screen name="LoginPage" component={LoginPage} />
        <Stack.Screen name="Store" component={Store} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
});

