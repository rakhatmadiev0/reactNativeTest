import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

const Filter = ({ selectedFilter, onSelectFilter }) => {
  return (
    <View style={styles.filtersContainer}>
      <TouchableOpacity
        style={[
          styles.filterButton,
          styles.brandFilter,
          selectedFilter === 'brand' && styles.selectedFilter,
        ]}
        onPress={() => onSelectFilter('brand')}
      >
        <Text>Бренд</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={[
          styles.filterButton,
          styles.categoryFilter,
          selectedFilter === 'category' && styles.selectedFilter,
        ]}
        onPress={() => onSelectFilter('category')}
      >
        <Text>Категория</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={[
          styles.filterButton,
          styles.TRUFilter,
          selectedFilter === 'TRU' && styles.selectedFilter,
        ]}
        onPress={() => onSelectFilter('TRU')}
      >
        <Text>ТРУ</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
    filtersContainer: {
      flexDirection: 'row',
      marginBottom: 20,
    },
    filterButton: {
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 3,
      borderWidth: 1,
      marginRight: 10,
      height: 24.8,
    },
   
  });

export default Filter;
