import axios from 'axios';

const baseURL = 'http://10.50.58.50:8001'; 

export const login = async (email, password) => {
  try {
    const response = await axios.post(
      `${baseURL}/api/login`, 
      { email, password },
      { headers: { 'Content-Type': 'application/json' } }
    );
    return response.data;
  } catch (error) {
    console.error(error);
  }
};
