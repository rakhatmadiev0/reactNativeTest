import React, { useState } from 'react';
import { View, TextInput, StyleSheet, TouchableOpacity, Text, Image } from 'react-native';

const Search = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const searchData = [
    
  ];

  const handleSearch = (query) => {
    const filteredData = searchData.filter((item) =>
      item.name.toLowerCase().includes(query.toLowerCase())
    );
    setSearchResults(filteredData);
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.searchContainer}
        onPress={() => {
          handleSearch(searchQuery);
        }}
      >
        <TextInput
          style={styles.input}
          placeholder="Поиск"
          onChangeText={(text) => {
            setSearchQuery(text);
            handleSearch(text);
          }}
          value={searchQuery}
        />
        <View style={styles.Icons}>
          <View style={styles.iconContainer}>
            <Image
              source={require('../../../assets/images/Vector (1).png')}
              style={styles.imageStyle}
            />
            <View style={styles.verticalLine}></View>
            <Image
              source={require('../../../assets/images/Vector.png')}
              style={styles.imageStyle}
            />
          </View>
        </View>
      </TouchableOpacity>

      <View style={styles.resultsContainer}>
        {searchResults.map((item) => (
          <Text key={item.id}>{item.name}</Text>
        ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 16,
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    flex: 1,
  },
  Icons: {
    display: 'flex',
    justifyContent: 'flex-end', 
  },
  iconContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
    borderWidth: 1,
    borderColor: 'gray',
  },
  verticalLine: {
    height: '100%',
    width: 1,
    backgroundColor: 'gray',
    marginHorizontal: 5,
  },
  imageStyle: {
    width: 20,
    height: 20,
    marginLeft: 3,
  },
  resultsContainer: {
    marginTop: 10,
  },
});

export default Search;
