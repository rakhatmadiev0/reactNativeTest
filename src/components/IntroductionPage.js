import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, Text, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import * as Font from 'expo-font';
import LoginPage from './LoginPage';
import { useNavigation } from '@react-navigation/native';
import { useFonts } from 'expo-font';



/* SVG IMPORTS */
import ImgLogo from '../../assets/icons/ImgLogo';
import Next from '../../assets/icons/Next';
import TextLogo from '../../assets/icons/TextLogo';
/* SVG IMPORTS */

export default function IntroductionPage() {
    let [fontsLoaded] = useFonts({
        'Serial': require('../../assets/fonts/PTSans-Regular.ttf')
    })
    
    if (!fontsLoaded) {
        return <Text>Loading fonts...</Text>;
    }


   const navigation = useNavigation();

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.rectangle}>
                    {/* <svg width="393" height="316" viewBox="0 0 393 316" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M6.14252 -13.9653H386.857V223.009C386.857 271.058 347.906 310.009 299.857 310.009H93.1426C45.0938 310.009 6.14252 271.058 6.14252 223.009V-13.9653Z" fill="#183961" stroke="white" stroke-width="12"/>
                    </svg> */}
                    <View style={styles.textBackground}>
                        <View style={styles.textWrapper}>
                            <Text style={styles.rectangleText}>
                                Дорогие пользователи,{"\n"}
                                мы рады приветствовать Вас {"\n"}в маркетплейсе{"\n"}
                                «Samruk Kazyna Store».
                            </Text>
                        </View>
                        <View style={styles.logo}>
                            <ImgLogo />
                        </View>
                    </View>
                </View>
            </View>

            <View style={styles.textLogo}>
                <TextLogo />
            </View>

            <View style={styles.btn}>
                <TouchableOpacity  onPress={() => navigation.navigate('LoginPage')}>
                    <Next />
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ECECEC',
    },
    header: {
        width: '100%',
        height: '47%',
        // position: 'relative',
        // left: 17
    },
    logo: {
        position: 'absolute',
        top: 240,
        display: 'flex',
        alignItems: 'center'
    },
    rectangle: {
        position: 'relative'
    },
    rectangleText: {
        fontFamily: 'PT Sans',
        fontSize: 23,
        fontWeight: '700',
        lineHeight: 30,
        letterSpacing: 1,
        textAlign: 'center',
        color: '#FFFFFF',
    },
    textBackground: {
        width: '100%', 
        height: 335.97,
        borderColor: '#FFFFFF',
        backgroundColor: '#183961',
        borderWidth: 10,
        borderTopWidth: 0,
        borderBottomLeftRadius: 80,
        borderBottomRightRadius: 80,
        display: 'flex',
        alignItems: 'center',
    },
    textWrapper: {
        width: '83%',
        marginTop: 60,
    },
    textLogo: {
        display: 'flex',
        alignItems: 'center',
        marginTop: 30
    },
    btn: {
        display: 'flex',
        alignItems: 'center',
        marginTop: 160
    }
});