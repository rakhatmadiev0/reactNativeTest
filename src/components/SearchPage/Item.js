import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';

const Item = ({ data }) => {
  const imageUrl = data.customImage ? data.customImage : data.image;

  return (
    <View style={styles.itemContainer}>
      <View style={styles.itemContent}>
        <Image source={require('../../../assets/images/item.png')} style={styles.itemImage} />
        <View style={styles.textContainer}>
          <Text style={styles.itemName} numberOfLines={2}>{data.name}</Text>
          <Text style={styles.itemBrand} numberOfLines={1}>{data.brand}</Text>
          <Text style={styles.itemCategory} numberOfLines={1}>{data.category}</Text>
          <View style={styles.Sayan}>
            <Text style={styles.itemPrice} numberOfLines={1}>Цена: {data.price} </Text>
            <TouchableOpacity style={styles.offerButton}>
            <Text style={styles.offerButtonText}>Предложить цену</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <Image source={require('../../../assets/images/star.png')} style={styles.starIcon} />
    </View>
  );
};

const styles = StyleSheet.create({
  itemContainer: {
    borderWidth: 1,
    borderColor: 'lightgray',
    borderRadius: 5,
    padding: 10,
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  itemContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemImage: {
    width: 100,
    height: 100,
    borderRadius: 5,
    marginRight: 10,
  },
  textContainer: {
    flex: 1,
  },
  itemName: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  itemBrand: {
    fontSize: 14,
    color: 'gray',
    marginBottom: 5,
  },
  itemCategory: {
    fontSize: 14,
    color: 'gray',
    marginBottom: 5,
  },
  itemPrice: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'green',
  },
  offerButton: {
    backgroundColor: '#AF9778',
    padding: 8,
    borderRadius: 5,
    marginLeft: 50,
  },
  offerButtonText: {
    color: 'white',
    fontWeight: 'bold',
  },
  starIcon: {
    width: 20,
    height: 20,
    position: 'absolute',
    right: 4,
  },
  Sayan:{
    display:'flex',
    flexDirection:'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  }
});

export default Item;
